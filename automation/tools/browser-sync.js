// Module load
const gulp = require('gulp'),
	browserSync = require('browser-sync').create(),
	options = require('../options');
function init() {
	browserSync.init({
		proxy: options.siteLocal,
		baseDir: ['./'],
		reloadDebounce: 1000
	});

	browserSync.watch(
		[
			'./**/dist/**/*.js',
			'./**/dist/**/*.css',
			'./**/*.html',
			'!./node_modules/**/*'
		],
		(evt, file) => {
			if (evt === 'change' && file.indexOf('.css') === -1) {
				browserSync.reload();
			}
			if (evt === 'change' && file.indexOf('.css') !== -1) {
				browserSync.reload('*.css');
			}
		}
	);
}

gulp.task('browsersync', init);
